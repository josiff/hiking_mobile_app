import 'package:flutter/widgets.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class Utils {
  static FormFieldValidator<String> passwordValidator(BuildContext context) {
    return FormBuilderValidators.compose([
      FormBuilderValidators.required(),
      FormBuilderValidators.match(r'^(?=.*?[A-Z])', errorText: "Big chars"),
      FormBuilderValidators.match(r'^(?=.*?[0-9])', errorText: "Number"),
      FormBuilderValidators.match( r'^(?=.*?[!@#\$&*~.])',
          errorText: "Spec"),
      FormBuilderValidators.minLength( 8, errorText: "Min number")
    ]);
  }
}
