import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:hiking_challenge_mobile/provider/auth_provider.dart';
import 'package:hiking_challenge_mobile/service/auth_service.dart';
import 'package:hiking_challenge_mobile/service/base_http_client.dart';
import 'package:hiking_challenge_mobile/service/navigation_service.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'hiking_exception.dart';

class HikingInterceptor extends Interceptor {
  final Dio _dio;

  HikingInterceptor(this._dio);

  @override
  void onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    final pref = await SharedPreferences.getInstance();
    final token = pref.getString(AuthService.token);
    // Do something before request is sent
    if (token != null) {
      _setToken(options, token);
    }
    handler.next(options);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    if (err.response?.statusCode == 401) {
      _refreshToken(err, handler);
    } else {
      handler.next(HikingException(err));
    }
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    handler.next(response);
  }

  Future<bool> isConnected() async {
    final connectivityResult = await Connectivity().checkConnectivity();
    return connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi;
  }

  Future<void> _refreshToken(
      DioError error, ErrorInterceptorHandler handler) async {
    final pref = await SharedPreferences.getInstance();
    final refreshToken = pref.getString(AuthService.refreshToken);
    if (refreshToken != null && refreshToken.isNotEmpty) {
      // _dio.interceptors.requestLock.lock(); TODO
      // _dio.interceptors.responseLock.lock();
      final data = {"refresh": refreshToken};
      RequestOptions requestOptions = error.response!.requestOptions;

      try {
        final response = await Dio()
            .post('${BaseHttpClient.baseUrl}/auth/token/refresh/', data: data);

        if (response.statusCode != 200) {
          Provider.of<AuthProvider>(
                  NavigationService.navigatorKey.currentContext!,
                  listen: false)
              .logout();
        }

        final token = response.data['access'];
        await pref.setString(AuthService.token, token);
        _setToken(requestOptions, token);
        // _dio.interceptors.requestLock.unlock(); TODO
        // _dio.interceptors.responseLock.unlock();
        final opts = Options(
            method: requestOptions.method, headers: requestOptions.headers);
        final requestResponse = await _dio.request(
          requestOptions.path,
          options: opts,
          queryParameters: requestOptions.queryParameters,
          data: requestOptions.data,
          cancelToken: requestOptions.cancelToken,
          onReceiveProgress: requestOptions.onReceiveProgress,
        );
        return handler.resolve(requestResponse);
      } on DioError catch (_) {
        Provider.of<AuthProvider>(
                NavigationService.navigatorKey.currentContext!,
                listen: false)
            .logout();
      }
    }
  }

  void _setToken(RequestOptions options, String? token) {
    options.headers["Authorization"] = "Bearer $token";
  }
}
