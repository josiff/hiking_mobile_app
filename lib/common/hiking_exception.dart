import 'package:dio/dio.dart';

class HikingException extends DioError {
  HikingException(DioError error)
      : super(
            error: error.error,
            requestOptions: error.requestOptions,
            response: error.response,
            type: error.type);

  @override
  String toString() {
    return message;
  }

  @override
  String get message {
    String errorMessage = super.message ?? "";
    switch (type) {
      case DioErrorType.cancel:
        errorMessage = "Request to API server was cancelled";
        break;
      case DioErrorType.connectionTimeout:
        errorMessage = "Connection timeout with API server";
        break;
      case DioErrorType.receiveTimeout:
        errorMessage = "Receive timeout in connection with API server";
        break;
      case DioErrorType.badResponse:
        errorMessage =
            _handleError(response?.statusCode);
        break;
      case DioErrorType.sendTimeout:
        errorMessage = "Send timeout in connection with API server";
        break;
      default:
        errorMessage = "Something went wrong";
        break;
    }

    return errorMessage;
  }

  String _handleError(int? statusCode) {
    switch (statusCode) {
      case 400:
        return "Bad request";
      case 401:
        return "Unauthorized";
      case 404:
        return "Not found";
      case 500:
        return "Internal server error";
      default:
        return "Oops something went wrong";
    }
  }
}
