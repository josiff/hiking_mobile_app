import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hiking_challenge_mobile/screen/login_screen/login_screen.dart';

import 'components/forgot_password_form.dart';

class ForgotPasswordScreen extends StatelessWidget {
  static const routeName = "/forgotPassword";

  const ForgotPasswordScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.all(10),
            height: deviceSize.height,
            width: deviceSize.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const ForgotPasswordForm(),
                const SizedBox(height: 20),
                InkWell(
                  onTap: () => Navigator.of(context)
                      .pushReplacementNamed(LoginScreen.routeName),
                  child: const Text("Cancel", style: TextStyle(color:  Colors.blue, fontWeight: FontWeight.bold),),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}