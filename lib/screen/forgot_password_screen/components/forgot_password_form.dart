import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:hiking_challenge_mobile/screen/login_screen/login_screen.dart';
import 'package:hiking_challenge_mobile/service/auth_service.dart';

class ForgotPasswordForm extends StatefulWidget {
  const ForgotPasswordForm({Key? key}) : super(key: key);

  @override
  _ForgotPasswordFormState createState() => _ForgotPasswordFormState();
}

class _ForgotPasswordFormState extends State<ForgotPasswordForm> {
  final AuthService _authService = AuthService();

  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  final bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        FormBuilder(
          key: _fbKey,
          child: FormBuilderTextField(
            name: "email",
            decoration: InputDecoration(
                hintText: AppLocalizations.of(context)!.loginPageEmail),
            validator: FormBuilderValidators.compose([
              FormBuilderValidators.required(),
            ]),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 10),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4), color: Colors.blue),
          child: MaterialButton(
            child: Container(
              alignment: Alignment.centerLeft,
              child: Center(
                child: _isLoading
                    ? const CircularProgressIndicator(
                        color: Colors.white38,
                      )
                    : const Text("Reset", style: TextStyle(color: Colors.white)),
              ),
            ),
            onPressed: () {
              if (_fbKey.currentState!.saveAndValidate()) {
                _recoverPassword(
                  context,
                  _fbKey.currentState!.fields['email']!.value.toString(),
                );
              }
            },
          ),
        )
      ],
    );
  }

  void _recoverPassword(BuildContext context, String email) async {
    try {
      await _authService.recoverPassword(email);
      Navigator.of(context).pushReplacementNamed(LoginScreen.routeName);
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text("Successs")));
    } catch (error) {
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text("Recover failed")));
    }
  }
}
