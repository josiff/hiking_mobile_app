import 'package:flutter/widgets.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapScreen extends StatefulWidget {
  const MapScreen({Key? key}) : super(key: key);

  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  late GoogleMapController mapController;

  final LatLng _center = const LatLng(48.148598, 17.107748);
  final List<Marker> markers = [];

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  @override
  void initState() {
    markers.add(Marker(markerId: const MarkerId("1"), position: _center));
    markers.add(const Marker(
        markerId: MarkerId("2"),
        position: LatLng(48.13442245553074, 17.117373098466825)));
    markers.add(const Marker(
        markerId: MarkerId("3"),
        position: LatLng(48.11602166461555, 17.156026165082775)));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GoogleMap(
      markers: markers.toSet(),
      myLocationEnabled: false,
      myLocationButtonEnabled: false,
      onMapCreated: _onMapCreated,
      initialCameraPosition: CameraPosition(
        target: _center,
        zoom: 11.0,
      ),
    );
  }
}
