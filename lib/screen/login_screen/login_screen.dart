import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:hiking_challenge_mobile/screen/forgot_password_screen/forgot_password_screen.dart';
import 'package:hiking_challenge_mobile/screen/ip_screen/ip_screen.dart';
import 'package:hiking_challenge_mobile/screen/login_screen/components/login_form.dart';
import 'package:hiking_challenge_mobile/screen/register_screen/register_screen.dart';

class LoginScreen extends StatelessWidget {
  static const routeName = "/";

  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.all(10),
            height: deviceSize.height,
            width: deviceSize.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const LoginForm(),
                const SizedBox(height: 20),
                InkWell(
                  onTap: () => Navigator.of(context)
                      .pushReplacementNamed(ForgotPasswordScreen.routeName),
                  child: Text(AppLocalizations.of(context)!
                      .loginPageRedirectToForgotButton, style: const TextStyle(color:  Colors.blue, fontWeight: FontWeight.bold),),
                ),
                const SizedBox(height: 20),
                const Divider(
                  height: 10,
                  color: Colors.black38,
                ),
                const SizedBox(height: 20),
                const Text("Already have an account?"),
                const SizedBox(height: 10),
                InkWell(
                  onTap: () => Navigator.of(context)
                      .pushReplacementNamed(RegisterScreen.routeName),
                  child: Text(AppLocalizations.of(context)!
                      .loginPageRedirectToRegisterButton),
                ),
                const SizedBox(height: 10),
                InkWell(
                  onTap: () =>
                      Navigator.of(context).pushNamed(IpScreen.routeName),
                  child: const Padding(
                    padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                    child: Text("IP"),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
