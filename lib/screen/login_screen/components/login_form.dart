import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:hiking_challenge_mobile/provider/auth_provider.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  final initialData = {"email": "admin@admin.com", "password": "admin"};

  bool _isLogging = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        FormBuilder(
          key: _fbKey,
          initialValue: initialData,
          child: Column(
            children: <Widget>[
              FormBuilderTextField(
                name: "email",
                decoration: InputDecoration(
                    hintText: AppLocalizations.of(context)!.loginPageEmail),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(),
                ]),
              ),
              FormBuilderTextField(
                name: "password",
                obscureText: true,
                decoration: InputDecoration(
                    hintText: AppLocalizations.of(context)!.loginPagePassword),
                // validator: Utils.passwordValidator(context), TODO uncomment
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(),
                ]),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 10),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4), color: Colors.blue),
          child: MaterialButton(
            child: Container(
              alignment: Alignment.centerLeft,
              child: Center(
                child: _isLogging
                    ? const CircularProgressIndicator(
                        color: Colors.white38,
                      )
                    : Text(AppLocalizations.of(context)!.loginPageLoginButton,
                        style: const TextStyle(color: Colors.white)),
              ),
            ),
            onPressed: () {
              if (_fbKey.currentState!.saveAndValidate()) {
                _login(
                  context,
                  _fbKey.currentState!.fields['email']!.value.toString(),
                  _fbKey.currentState!.fields['password']!.value.toString(),
                );
              }
            },
          ),
        )
      ],
    );
  }

  void _setLoading(bool flag) {
    setState(() {
      _isLogging = flag;
    });
  }

  void _login(BuildContext context, String email, String password) async {
    try {
      _setLoading(true);
      await Provider.of<AuthProvider>(context, listen: false)
          .login(email, password);
    } catch (error) {
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text("Login faild")));
    } finally {
      _setLoading(false);
    }
  }
}
