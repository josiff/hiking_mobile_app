import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hiking_challenge_mobile/common/location_service.dart';
import 'package:hiking_challenge_mobile/model/challenge.dart';
import 'package:hiking_challenge_mobile/screen/challenge_detail/components/challenge_detail_bottom.dart';
import 'package:hiking_challenge_mobile/screen/challenge_detail/components/image_slider.dart';
import 'package:hiking_challenge_mobile/screen/map_screen.dart';
import 'package:hiking_challenge_mobile/service/challenge_service.dart';

class ChallengeDetailScreen extends StatefulWidget {
  final Challenge challenge;

  const ChallengeDetailScreen(this.challenge, {Key? key}) : super(key: key);

  @override
  _ChallengeDetailScreenState createState() => _ChallengeDetailScreenState();
}

class _ChallengeDetailScreenState extends State<ChallengeDetailScreen> {
  final challengeService = ChallengeService();
  final locationService = LocationService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            ImageSlider(widget.challenge.name),
            const SizedBox(height: 30),
            Row(
              children: [
                _shortInfo("Distance", "4.5km", Icons.directions_bike_outlined),
                _shortInfo("Elevation", "1850m", Icons.elevator),
                _shortInfo("Difficulty", "5", Icons.ac_unit_outlined),
              ],
            ),
            const SizedBox(height: 30),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Center(
                child: Text(
                  widget.challenge.description,
                ),
              ),
            ),
            const SizedBox(height: 30),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 8.0),
              child: const MapScreen(),
              height: 220,
            ),
          ],
        ),
      ),
      bottomNavigationBar: ChallengeDetailBottom(widget.challenge),
    );
  }

  Expanded _shortInfo(String title, String value, IconData icon) {
    return Expanded(
      flex: 3,
      child: Column(
        children: [
          Icon(icon),
          const SizedBox(height: 5),
          Text(title),
          const SizedBox(height: 10),
          Text(
            value,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ],
      ),
    );
  }
}
