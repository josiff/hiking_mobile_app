import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hiking_challenge_mobile/model/challenge.dart';

class ChallengeDetailBottom extends StatelessWidget {
  final Challenge challenge;

  const ChallengeDetailBottom(this.challenge, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 110,
      padding: const EdgeInsets.only(bottom: 20, left: 20, right: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "Total \$${challenge.price}",
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
          ),
          Container(
            alignment: Alignment.center,
            child: const Text(
              "Go",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 20),
            ),
            decoration:
                const BoxDecoration(shape: BoxShape.circle, color: Colors.blueGrey),
            width: 110,
            height: 110,
          ),
        ],
      ),
    );
  }
}
