import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hiking_challenge_mobile/provider/auth_provider.dart';
import 'package:hiking_challenge_mobile/provider/chellenge_provider.dart';
import 'package:hiking_challenge_mobile/screen/map_screen.dart';
import 'package:provider/provider.dart';

import '../widget/bottom_bar.dart';
import 'dashboard/dashboard_screen.dart';

class BaseScreen extends StatefulWidget {
  static const routeName = "/baseScreen";

  const BaseScreen({Key? key}) : super(key: key);

  @override
  _BaseScreenState createState() => _BaseScreenState();
}

class _BaseScreenState extends State<BaseScreen> {
  int tabIndex = 0;
  bool _isInit = true;

  @override
  void didChangeDependencies() {
    if (_isInit) {
      _loadProjects();
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  void _loadProjects() async {
    await Provider.of<ChallengeProvider>(context, listen: false)
        .fetchChallenges();
  }

  void onTabPress(int selectedIndex) {
    setState(() {
      tabIndex = selectedIndex;
    });
  }

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);

    final List<Widget> tabScreens = [
      const DashboardScreen(),
      const MapScreen(),
      const Center(
        child: Text("Tab 3 "),
      ),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            InkWell(
              onTap: () {
                authProvider.logout();
              },
              child: const SizedBox(
                height: 40,
                child: Card(
                  child: Center(
                    child: Text("Odhlasit"),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 20),
            const Center(
              child: Text("Tab 4 "),
            ),
          ],
        ),
      ),
    ];
    return Scaffold(
      body: tabScreens[tabIndex],
      bottomNavigationBar: BottomBar(tabIndex, onTabPress),
    );
  }
}
