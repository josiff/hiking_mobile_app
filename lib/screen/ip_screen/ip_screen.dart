import 'package:flutter/material.dart';
import 'package:hiking_challenge_mobile/screen/ip_screen/components/ip_form.dart';

class IpScreen extends StatelessWidget {
  static const routeName = "/ipScreen";

  const IpScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.all(10),
            height: deviceSize.height,
            width: deviceSize.width,
            child: const IpForm(),
          ),
        ),
      ),
    );
  }
}
