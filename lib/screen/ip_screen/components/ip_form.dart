import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:hiking_challenge_mobile/provider/ip_provider.dart';
import 'package:hiking_challenge_mobile/service/base_http_client.dart';
import 'package:provider/provider.dart';

class IpForm extends StatefulWidget {
  const IpForm({Key? key}) : super(key: key);

  @override
  State<IpForm> createState() => _IpFormState();
}

class _IpFormState extends State<IpForm> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  final initialData = {"ip": BaseHttpClient.baseUrl};

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        FormBuilder(
          key: _fbKey,
          initialValue: initialData,
          child: Column(
            children: <Widget>[
              FormBuilderTextField(
                name: "ip",
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 10),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4), color: Colors.blue),
          child: MaterialButton(
            child: Container(
              alignment: Alignment.centerLeft,
              child: const Center(
                child: Text("Set IP", style: TextStyle(color: Colors.white)),
              ),
            ),
            onPressed: () async {
              if (_fbKey.currentState!.saveAndValidate()) {
                final ip = _fbKey.currentState!.fields['ip']!.value.toString();
                await Provider.of<IpProvider>(context, listen: false)
                    .updateIp(ip);
                ScaffoldMessenger.of(context)
                    .showSnackBar(const SnackBar(content: Text("Ip Changed")));
              }
            },
          ),
        )
      ],
    );
  }
}
