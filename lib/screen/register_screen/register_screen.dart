import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:hiking_challenge_mobile/screen/login_screen/login_screen.dart';

import 'components/register_form.dart';

class RegisterScreen extends StatelessWidget {
  static const routeName = "/register";

  const RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.all(10),
            height: deviceSize.height,
            width: deviceSize.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const RegisterForm(),
                const SizedBox(height: 20),
                const Divider(
                  height: 10,
                  color: Colors.black38,
                ),
                const SizedBox(height: 20),
                const Text("Already have an account?"),
                const SizedBox(height: 10),
                InkWell(
                  onTap: () => Navigator.of(context).pushReplacementNamed(LoginScreen.routeName),
                  child: Padding(
                    padding:
                        const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                    child: Text(AppLocalizations.of(context)!
                        .registerPageRedirectToLoginButton),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
