import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:hiking_challenge_mobile/common/utils.dart';
import 'package:hiking_challenge_mobile/model/register.dart';
import 'package:hiking_challenge_mobile/screen/login_screen/login_screen.dart';
import 'package:hiking_challenge_mobile/service/auth_service.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class RegisterForm extends StatefulWidget {
  const RegisterForm({Key? key}) : super(key: key);

  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  final AuthService authService = AuthService();

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        FormBuilder(
          key: _fbKey,
          child: Column(
            children: <Widget>[
              FormBuilderTextField(
                name: "username",
                decoration: InputDecoration(
                    hintText:
                        AppLocalizations.of(context)!.registerPageUserName),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(),
                ]),
              ),
              FormBuilderTextField(
                name: "first_name",
                decoration: InputDecoration(
                    hintText:
                        AppLocalizations.of(context)!.registerPageFirstName),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(),
                ]),
              ),
              FormBuilderTextField(
                name: "last_name",
                decoration: InputDecoration(
                    hintText:
                        AppLocalizations.of(context)!.registerPageLastName),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(),
                ]),
              ),
              FormBuilderTextField(
                name: "email",
                decoration: InputDecoration(
                    hintText: AppLocalizations.of(context)!.loginPageEmail),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(),
                ]),
              ),
              FormBuilderTextField(
                name: "password",
                obscureText: true,
                decoration: InputDecoration(
                    hintText: AppLocalizations.of(context)!.loginPagePassword),
                validator: Utils.passwordValidator(context),
              ),
              FormBuilderCheckbox(
                initialValue: false,
                name: "rules",
                title: RichText(
                  text: const TextSpan(
                    children: [
                      TextSpan(
                        text: 'I have read and agree to the ',
                        style: TextStyle(color: Colors.black),
                      ),
                      TextSpan(
                        text: 'Terms and Conditions',
                        style: TextStyle(color: Colors.blue),
                      ),
                    ],
                  ),
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(),
                ]),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 10),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4), color: Colors.blue),
          child: MaterialButton(
            child: Container(
              alignment: Alignment.centerLeft,
              child: Center(
                child: Text(
                    AppLocalizations.of(context)!.registerPageRegisterButton,
                    style: const TextStyle(color: Colors.white)),
              ),
            ),
            onPressed: () {
              if (_fbKey.currentState!.saveAndValidate()) {
                _register(
                  context,
                  Register.fromJson(_fbKey.currentState!.value),
                );
              }
            },
          ),
        )
      ],
    );
  }

  void _register(BuildContext context, Register register) async {
    try {
      await authService.register(register);
      Navigator.of(context).pushReplacementNamed(LoginScreen.routeName);
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text("Successs")));
    } catch (error) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(error.toString())));
    }
  }
}
