import 'package:flutter/material.dart';
import 'package:hiking_challenge_mobile/provider/chellenge_provider.dart';
import 'package:hiking_challenge_mobile/screen/dashboard/components/challenge_list.dart';
import 'package:hiking_challenge_mobile/screen/dashboard/components/dashboard_header.dart';
import 'package:hiking_challenge_mobile/widget/section_title.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class DashboardScreen extends StatefulWidget {
  static const routeName = "/dashboard";

  const DashboardScreen({Key? key}) : super(key: key);

  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  @override
  Widget build(BuildContext context) {
    return Consumer<ChallengeProvider>(
      builder: (ctx, challengeProvider, child) => SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 20),
              const DashboardHeader(),
              const SizedBox(height: 20),
              SectionTitle(title: AppLocalizations.of(context)!.dashboardPageMyChallenges, press: () {}),
              ChallengeList(challengeProvider.myChallenges),
              const SizedBox(height: 20),
              SectionTitle(title: AppLocalizations.of(context)!.dashboardPageAllChallenges, press: () {}),
              ChallengeList(challengeProvider.allChallenges),
            ],
          ),
        ),
      ),
    );
  }
}
