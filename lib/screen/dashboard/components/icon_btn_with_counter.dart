import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class IconBtnWithCounter extends StatelessWidget {
  const IconBtnWithCounter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: 40,
          width: 40,
          decoration: BoxDecoration(
            color: const Color(0xFF979797).withOpacity(0.1),
            shape: BoxShape.circle,
          ),
          child: const Icon(Icons.notifications),
        ),
        Positioned(
          top: 0,
          right: 0,
          child: Container(
            height: 16,
            width: 16,
            decoration: BoxDecoration(
              color: const Color(0xFFFF4848),
              shape: BoxShape.circle,
              border: Border.all(width: 1.5, color: Colors.white),
            ),
          ),
        )
      ],
    );
  }
}
