import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:hiking_challenge_mobile/model/challenge.dart';
import 'package:hiking_challenge_mobile/provider/chellenge_provider.dart';
import 'package:hiking_challenge_mobile/widget/challenge_widget.dart';
import 'package:provider/provider.dart';

class ChallengeList extends StatelessWidget {
  final List<Challenge> challenges;

  const ChallengeList(this.challenges, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 265,
      child: Consumer<ChallengeProvider>(
        builder: (ctx, challengeProvider, child) => ListView.builder(
          scrollDirection: Axis.horizontal,
          itemBuilder: (ctx, index) => Container(
              margin: const EdgeInsets.symmetric(horizontal: 2),
              child: ChallengeWidget(challenges[index])),
          itemCount: challenges.length,
        ),
      ),
    );
  }
}
