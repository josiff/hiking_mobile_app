import 'package:flutter/widgets.dart';
import 'package:hiking_challenge_mobile/service/base_http_client.dart';
import 'package:shared_preferences/shared_preferences.dart';

class IpProvider with ChangeNotifier {
  Future<void> setIp() async {
    final prefs = await SharedPreferences.getInstance();
    BaseHttpClient.baseUrl =
        prefs.getString(BaseHttpClient.urlKey) ?? BaseHttpClient.baseUrl;
  }

  Future<void> updateIp(String ip) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(BaseHttpClient.urlKey, ip);
    BaseHttpClient.baseUrl = ip;
    notifyListeners();
  }
}
