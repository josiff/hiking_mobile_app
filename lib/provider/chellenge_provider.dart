import 'package:flutter/widgets.dart';
import 'package:hiking_challenge_mobile/model/challenge.dart';
import 'package:hiking_challenge_mobile/service/challenge_service.dart';

class ChallengeProvider with ChangeNotifier {
  final ChallengeService _challengeService = ChallengeService();
  final List<Challenge> _challenges = [];
  final List<Challenge> _myChallenges = [];

  Future<void> fetchChallenges() async {
    final challengesFromDb = await _challengeService.fetchFromDb();
    addAll(challengesFromDb);
    final challenges = await _challengeService.loadAllFromServer();
    _challenges.clear();
    addAll(challenges);
  }

  void addAll(List<Challenge> challenges) {
    _challenges.addAll(
        challenges.where((challenge) => challenge.id! < 2)); // TODO demo
    _myChallenges.addAll(challenges.where((challenge) => challenge.id! > 1));
    notifyListeners();
  }

  Challenge findById(int itemId) {
    return _challenges.firstWhere((element) => element.id == itemId);
  }

  List<Challenge> get allChallenges {
    return _challenges;
  }

  List<Challenge> get myChallenges {
    return _myChallenges;
  }
}
