import 'dart:async';
import 'package:flutter/widgets.dart';
import 'package:hiking_challenge_mobile/service/auth_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthProvider with ChangeNotifier {
  bool _isAuth = false;

  final AuthService _authService = AuthService();

  Future<void> login(String email, String password) async {
    await _authService.authenticate(email, password);
    tryAutoLogin();
  }

  Future<void> tryAutoLogin() async {
    final prefs = await SharedPreferences.getInstance();
    _isAuth = prefs.containsKey(AuthService.token);
    if (_isAuth) {
      notifyListeners();
    }
  }

  Future<void> logout() async {
    _isAuth = false;
    await _authService.logout();
    notifyListeners();
  }

  bool get isAuth => _isAuth;
}
