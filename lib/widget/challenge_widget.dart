import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hiking_challenge_mobile/model/challenge.dart';
import 'package:hiking_challenge_mobile/screen/challenge_detail/challenge_detail_screen.dart';
import 'package:intl/intl.dart';

class ChallengeWidget extends StatelessWidget {
  final Challenge challenge;

  const ChallengeWidget(this.challenge, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Navigator.push(
        context,
        PageRouteBuilder(
          transitionDuration: const Duration(milliseconds: 500),
          reverseTransitionDuration: const Duration(milliseconds: 500),
          pageBuilder: (context, animation, secondaryAnimation) =>
              FadeTransition(
            opacity: animation,
            child: ChallengeDetailScreen(challenge),
          ),
        ),
      ),
      child: SizedBox(
        width: 200,
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30),
          ),
          child: Container(
            padding: const EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _imageView(challenge.name),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 15),
                      Text(
                        challenge.name,
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),
                      ),
                      const SizedBox(height: 5),
                      Text(
                        challenge.description,
                        overflow: TextOverflow.ellipsis,
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  String _getPrice(double price) {
    final format = NumberFormat.currency(symbol: "\$", decimalDigits: 0);
    return format.format(price);
  }

  ClipRRect _imageView(String name) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: Stack(
        children: [
          Hero(
            tag: name,
            child: Image.network(
              "https://p0.pxfuel.com/preview/512/396/3/big-nature-mountains-view.jpg",
              fit: BoxFit.fitWidth,
              width: 200,
              height: 150,
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: Container(
              margin: const EdgeInsets.only(right: 10, top: 10),
              height: 40,
              width: 80,
              decoration: BoxDecoration(
                color: const Color(0xFF979797).withOpacity(0.7),
                borderRadius: BorderRadius.circular(10),
              ),
              child: Center(
                child: Text(
                  _getPrice(challenge.price),
                  style: const TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
