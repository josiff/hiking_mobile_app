import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SectionTitle extends StatelessWidget {
  const SectionTitle({Key? key,
    this.isMainSection = true,
    required this.title,
    required this.press,
  }) : super(key: key);

// Main Section means on Home page section
  final bool isMainSection;
  final String title;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            isMainSection ? title : title.toUpperCase(),
            style: isMainSection
                ? const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w600,
                    letterSpacing: 0.14,
                  )
                : const TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                    // height: 1.5,
                  ),
          ),
          GestureDetector(
            onTap: press,
            child: Text(
              isMainSection
                  ? AppLocalizations.of(context)!.seeAll
                  : AppLocalizations.of(context)!.clearAll.toUpperCase(),
              style: isMainSection
                  ? Theme.of(context).textTheme.bodyText2
                  : const TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w500,
                    ),
            ),
          ),
        ],
      ),
    );
  }
}
