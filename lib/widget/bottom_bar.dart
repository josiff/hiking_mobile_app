import 'package:flutter/material.dart';

import '../widget/tab_bottom_button.dart';

class BottomBar extends StatelessWidget {
  final int currentIndex;
  final Function onTapPress;

  const BottomBar(this.currentIndex, this.onTapPress, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 90,
      padding: const EdgeInsets.only(bottom: 30),
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          TabBottomButton(0, Icons.explore, currentIndex, onTapPress),
          TabBottomButton(1, Icons.search, currentIndex, onTapPress),
          TabBottomButton(2, Icons.notifications, currentIndex, onTapPress),
          TabBottomButton(3, Icons.settings, currentIndex, onTapPress),
        ],
      ),
    );

   // return BottomNavigationBar(
   //   type: BottomNavigationBarType.fixed,
   //   backgroundColor: Colors.white,
   //   items: <BottomNavigationBarItem>[
   //     buildBottomNavigationBarItem(Icons.av_timer),
   //     buildBottomNavigationBarItem(Icons.business),
   //     buildBottomNavigationBarItem(Icons.school),
   //     buildBottomNavigationBarItem(Icons.trending_up),
   //   ],
   //   currentIndex: currentIndex,
   //   showSelectedLabels: false,
   //   showUnselectedLabels: false,
   //   unselectedItemColor: Colors.black54,
   //   selectedItemColor: Theme.of(context).primaryColor,
   //   onTap: onTapPress,
   // );
  }

  BottomNavigationBarItem buildBottomNavigationBarItem(IconData icon) {
    return BottomNavigationBarItem(
      icon: Icon(icon),
      label: "",
//      backgroundColor: Colors.white,
    );
  }
}
