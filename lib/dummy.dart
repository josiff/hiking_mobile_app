const DUMMY_CHALLENGES = [
  {
    "id": 1,
    "name": "First Challenge Bern",
    "description": "Small descritption Bern",
    "short_description": "Short description Bern",
    "price": 25.0,
    "country": "CH",
    "start_date": "2021-11-12",
    "end_date": "2021-11-12",
    "registration_start_date": "2021-11-12",
    "registration_end_date": "2021-11-12",
    "created_at": "2021-11-12T10:06:22.803655Z",
    "updated_at": "2021-11-12T10:19:09.135719Z",
    "deleted_at": null
  },
  {
    "id": 2,
    "name": "First Challenge Bern",
    "description": "Small descritption Bern",
    "short_description": "Short description Bern",
    "price": 25.0,
    "country": "CH",
    "start_date": "2021-11-12",
    "end_date": "2021-11-12",
    "registration_start_date": "2021-11-12",
    "registration_end_date": "2021-11-12",
    "created_at": "2021-11-12T10:06:22.803655Z",
    "updated_at": "2021-11-12T10:19:09.135719Z",
    "deleted_at": null
  }
];
