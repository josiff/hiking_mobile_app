import 'package:hiking_challenge_mobile/screen/base_screen.dart';
import 'package:hiking_challenge_mobile/screen/forgot_password_screen/forgot_password_screen.dart';
import 'package:hiking_challenge_mobile/screen/ip_screen/ip_screen.dart';
import 'package:hiking_challenge_mobile/screen/register_screen/register_screen.dart';

final appRoutes = {
  ForgotPasswordScreen.routeName: (context) => const ForgotPasswordScreen(),
  RegisterScreen.routeName: (context) => const RegisterScreen(),
  BaseScreen.routeName: (context) => const BaseScreen(),
  IpScreen.routeName: (context) => const IpScreen(),
};
