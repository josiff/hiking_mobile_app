// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'register.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Register _$RegisterFromJson(Map<String, dynamic> json) {
  return Register(
    json['first_name'] as String,
    json['last_name'] as String,
    json['password'] as String,
    json['email'] as String,
    json['username'] as String,
  );
}

Map<String, dynamic> _$RegisterToJson(Register instance) => <String, dynamic>{
      'first_name': instance.firstName,
      'last_name': instance.lastName,
      'password': instance.password,
      'email': instance.email,
      'username': instance.username,
    };
