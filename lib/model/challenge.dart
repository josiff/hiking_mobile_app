import 'package:hiking_challenge_mobile/model/audit_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'challenge.g.dart';

@JsonSerializable()
class Challenge extends AuditModel {
  int? id;
  String name;
  String description;

  // String distance;
  // String elevation;
  // String difficulty;
  String shortDescription;
  double price;
  String country;
  DateTime startDate;
  DateTime endDate;
  DateTime registrationStartDate;
  DateTime registrationEndDate;

  Challenge(
      this.id,
      this.name,
      this.description,
      this.shortDescription,
      this.price,
      this.country,
      this.startDate,
      this.endDate,
      this.registrationStartDate,
      this.registrationEndDate);


  factory Challenge.fromJson(Map<String, dynamic> json) =>
      _$ChallengeFromJson(json);

  Map<String, dynamic> toJson() => _$ChallengeToJson(this);
}
