// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'challenge.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Challenge _$ChallengeFromJson(Map<String, dynamic> json) {
  return Challenge(
    json['id'] as int?,
    json['name'] as String,
    json['description'] as String,
    json['short_description'] as String,
    (json['price'] as num).toDouble(),
    json['country'] as String,
    DateTime.parse(json['start_date'] as String),
    DateTime.parse(json['end_date'] as String),
    DateTime.parse(json['registration_start_date'] as String),
    DateTime.parse(json['registration_end_date'] as String),
  )
    ..createdAt = json['created_at'] == null
        ? null
        : DateTime.parse(json['created_at'] as String)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..deletedAt = json['deleted_at'] == null
        ? null
        : DateTime.parse(json['deleted_at'] as String);
}

Map<String, dynamic> _$ChallengeToJson(Challenge instance) => <String, dynamic>{
      'created_at': instance.createdAt?.toIso8601String(),
      'updated_at': instance.updatedAt?.toIso8601String(),
      'deleted_at': instance.deletedAt?.toIso8601String(),
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'short_description': instance.shortDescription,
      'price': instance.price,
      'country': instance.country,
      'start_date': instance.startDate.toIso8601String(),
      'end_date': instance.endDate.toIso8601String(),
      'registration_start_date':
          instance.registrationStartDate.toIso8601String(),
      'registration_end_date': instance.registrationEndDate.toIso8601String(),
    };
