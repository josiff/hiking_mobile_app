import 'package:json_annotation/json_annotation.dart';

part 'register.g.dart';

@JsonSerializable()
class Register {
  String firstName;
  String lastName;
  String password;
  String email;
  String username;

  Register(
      this.firstName, this.lastName, this.password, this.email, this.username);

  factory Register.fromJson(Map<String, dynamic> json) =>
      _$RegisterFromJson(json);

  Map<String, dynamic> toJson() => _$RegisterToJson(this);
}
