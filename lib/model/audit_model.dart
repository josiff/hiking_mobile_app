import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
abstract class AuditModel {
  DateTime? createdAt;
  DateTime? updatedAt;
  DateTime? deletedAt;

  AuditModel();
}
