import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:hiking_challenge_mobile/provider/auth_provider.dart';
import 'package:hiking_challenge_mobile/provider/chellenge_provider.dart';
import 'package:hiking_challenge_mobile/provider/ip_provider.dart';
import 'package:hiking_challenge_mobile/routes.dart';
import 'package:hiking_challenge_mobile/screen/base_screen.dart';
import 'package:hiking_challenge_mobile/screen/login_screen/login_screen.dart';
import 'package:hiking_challenge_mobile/screen/splash_screen.dart';
import 'package:hiking_challenge_mobile/service/navigation_service.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const HikingApp());
}

class HikingApp extends StatelessWidget {
  static const supportedLocales = [
    Locale('en'),
    Locale('sk'),
  ];

  const HikingApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => IpProvider(),
      child: Consumer<IpProvider>(
        builder: (ctx, ipProvider, _) => MultiProvider(
          providers: [
            ChangeNotifierProvider<AuthProvider>.value(value: AuthProvider()),
            ChangeNotifierProvider<ChallengeProvider>.value(
                value: ChallengeProvider()),
          ],
          child: MaterialApp(
            debugShowCheckedModeBanner: false,
            navigatorKey: NavigationService.navigatorKey,
            localizationsDelegates: const [
              AppLocalizations.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate
            ],
            supportedLocales: HikingApp.supportedLocales,
            onGenerateTitle: (BuildContext context) =>
                AppLocalizations.of(context)!.title,
            home: FutureBuilder(
              future: ipProvider.setIp(),
              builder: (ctx, authResultSnapshot) =>
                  authResultSnapshot.connectionState == ConnectionState.waiting
                      ? const SplashScreen()
                      : _app(),
            ),
            routes: appRoutes,
          ),
        ),
      ),
    );
  }

  Consumer _app() {
    return Consumer<AuthProvider>(
      builder: (ctx, authProvider, _) => authProvider.isAuth
          ? const BaseScreen()
          : FutureBuilder(
              future: authProvider.tryAutoLogin(),
              builder: (ctx, authResultSnapshot) =>
                  authResultSnapshot.connectionState == ConnectionState.waiting
                      ? const SplashScreen()
                      : const LoginScreen(),
            ),
    );
  }
}
