import 'package:hiking_challenge_mobile/dummy.dart';
import 'package:hiking_challenge_mobile/model/challenge.dart';
import 'package:hiking_challenge_mobile/service/base_http_client.dart';

class ChallengeService extends BaseHttpClient {
  ChallengeService() : super("/api/v1/challenges/");

  @override
  Future<List<Challenge>> findAll() async {
    // List<dynamic> list = await super.findAll();
    // return list.map((item) => Challenge.fromJson(item)).toList();

    return Future.delayed(const Duration(seconds: 1), () =>
        DUMMY_CHALLENGES.map((item) => Challenge.fromJson(item)).toList());
  }

  @override
  Future findById(int itemId) async {
    dynamic object = await super.findById(itemId);
    return Challenge.fromJson(object);
  }

  Future<List<Challenge>> fetchFromDb() {
    return Future.delayed(const Duration(milliseconds: 100)).then((_) {
      return [];
    });
  }

  Future<List<Challenge>> loadAllFromServer() async {
    //  todo save to db
    return findAll();
  }

}
