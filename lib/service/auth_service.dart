import 'package:hiking_challenge_mobile/model/register.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'base_http_client.dart';

class AuthService extends BaseHttpClient {
  static const token = 'token';
  static const refreshToken = 'refresh';

  AuthService() : super('/auth');

  Future<void> authenticate(String email, String password) async {
    // final data = {"email": email, "password": password, "stay_logged_in": true};
    //
    // final response = await dio.post('$route/token/', data: data);
    // final token = response.data['access'];
    // final refresh = response.data['refresh'];

    const token = "token";
    const refresh = "refreshToken";
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(AuthService.token, token);
    await prefs.setString(AuthService.refreshToken, refresh);
  }

  Future<void> logout() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(AuthService.refreshToken);
    prefs.remove(AuthService.token);
  }

  Future<dynamic> register(Register register) async {
    return dio.post('$route/user/', data: register.toJson());
  }

  Future<dynamic> getUser() async {
    return dio.get('$route/user/');
  }

  Future<dynamic> recoverPassword(String email) {
    final data = {"email": email};
    return dio.post('$route/user/reset-password/request/', data: data);
  }
}
