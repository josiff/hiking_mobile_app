import 'package:dio/dio.dart';
import 'package:hiking_challenge_mobile/common/hiking_interceptor.dart';

class BaseHttpClient {
  final String route;
  final Dio dio = Dio();
  static String baseUrl = 'http://0.0.0.0:8000';
  static const String urlKey = 'baseUrl';

  BaseHttpClient(this.route) {
    dio.options.baseUrl = BaseHttpClient.baseUrl;
    dio.interceptors.add(LogInterceptor(responseBody: false));
    dio.interceptors.add(HikingInterceptor(dio));
  }

  Future findAll() async {
    final response = await dio.get(route);
    return response.data;
  }

  Future findById(int itemId) async {
    final response = await dio.get("$route/$itemId");
    return response.data;
  }

  Future save(data) async {
    return dio.post(route, data: data);
  }
}
