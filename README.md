# hiking_challenge_mobile

app for hiking

## For Developers

For development is required flutter lib
- Instruction how to set up environment : [Flutter Get started](https://flutter.dev/docs/get-started/install)

Hints
- generate *.g.dart file `flutter pub run build_runner build`
- install dependencies `flutter pub get`
- run app `flutter run [--release]`
- build android `flutter build apk`
- code style `flutter analyze` or on the bottom in IDE is button [Dart Analysis]()

## For Testers
- Android app is available for development and master branch
  - each build(pipeline) contains app-release.apk, where you can download it and install to the phone
  - in the phone settings has to be allowed to install 3-party apps(Phone Settings -> Special access -> Install unknown apps) 


